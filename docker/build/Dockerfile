FROM golang:1.10

# Used to build ContinuousEvolution project, from golang, add docker, docker-compose and tools to make doc.
# Push a new version :
# > docker login registry.gitlab.com
# > docker build -t registry.gitlab.com/continuousevolution/continuous-evolution/build .
# > docker push registry.gitlab.com/continuousevolution/continuous-evolution/build

##
# Docker
##
RUN set -x \
	&& curl -fsSL get.docker.com -o get-docker.sh \
	&& sh get-docker.sh \
	&& docker -v

##
# Docker compose
##
ENV DOCKER_COMPOSE_VERSION 1.15.0
RUN curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose \
    && docker-compose -v

##
# Golang dep
##
RUN curl -L "https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64" -o ${GOPATH}/bin/dep \
	&& chmod +x ${GOPATH}/bin/dep \
	&& dep version

##
# Doc hugo
##
RUN curl -L "https://github.com/gohugoio/hugo/releases/download/v0.34/hugo_0.34_Linux-64bit.tar.gz" -o /tmp/hugo.tar.gz \
	&& mkdir /tmp/hugo && tar xf /tmp/hugo.tar.gz -C /tmp/hugo \
	&& mv /tmp/hugo/hugo /usr/local/bin/hugo \
    && rm /tmp/hugo.tar.gz && rm -rf /tmp/hugo \
    && chmod +x /usr/local/bin/hugo \
	&& hugo version

##
# Doc others
##
RUN go get github.com/robertkrimen/godocdown/godocdown \
	&& go get github.com/tdewolff/minify/cmd/minify
