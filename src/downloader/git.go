package downloader

import (
	"continuous-evolution/src/project"
	"regexp"
	"time"

	"github.com/sirupsen/logrus"
)

//GitConfig is configuration required by git downloader
type GitConfig struct {
	Enabled bool
}

//DefaultGitConfig is the default configuration required by git downloader
var DefaultGitConfig = GitConfig{Enabled: true}

var loggerGit = logrus.WithField("logger", "downloader/git")
var regexpProject = regexp.MustCompile(`https://([^:]+):([^@]+)@([^/]+)/([^/]+)/([^\.]+)\.git`)

type git struct {
	config Config
}

func newGit(config Config) downloader {
	return git{config}
}

func (g git) accept(url string) bool {
	return len(regexpProject.FindStringSubmatch(url)) == 6
}

func (g git) buildProject(gitURL string) (project.Project, error) {
	result := regexpProject.FindStringSubmatch(gitURL)

	login := result[1]
	token := result[2]
	typeHost := result[3]
	organisation := result[4]
	projectName := result[5]

	loggerGit.WithFields(logrus.Fields{
		"gitUrl":       gitURL,
		"typeHost":     typeHost,
		"organisation": organisation,
		"projectName":  projectName,
	}).Info("New project")

	return project.Project{
		CreatedDate:     time.Now(),
		LastUpdatedDate: time.Now(),
		Name:            projectName,
		GitURL:          gitURL,
		BranchName:      g.config.BranchName,
		PathToWrite:     g.config.PathToWrite,
		Login:           login,
		Token:           token,
		TypeHost:        typeHost,
		Organisation:    organisation,
	}, nil
}

func (g git) download(project project.Project) (project.Project, error) {
	loggerGit.WithField("project", project.GitURL).Info("Start downloading project")

	branchAlreadyExist, err := project.Git().BranchAlreadyExist()
	if err != nil {
		loggerGit.WithField("project", project).WithError(err).Error("Can't get if merge request already exist")
		return project, err
	}
	project = project.SetBranchAlreadyExist(branchAlreadyExist)

	if err := project.Git().Clone(); err != nil {
		loggerGit.Error(err)
		return project, err
	}

	return project, nil
}
