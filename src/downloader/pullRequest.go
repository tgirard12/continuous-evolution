package downloader

import (
	"continuous-evolution/src/project"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

//PullRequestConfig is configuration required by pull-request downloader
type PullRequestConfig struct {
	Enabled bool
}

//DefaultPullRequestConfig is the default configuration required by pull-request downloader
var DefaultPullRequestConfig = PullRequestConfig{Enabled: true}

var loggerPullRequest = logrus.WithField("logger", "downloader/pullRequest")
var regexpPullRequest = regexp.MustCompile(`https://([^:]+):([^@]+)@api.github.com/repos/([^/]+)/([^/]+)/pulls/(.+)`)

type pullRequest struct {
	config  Config
	deleter DeleteProject
}

func newPullRequest(config Config, deleter DeleteProject) downloader {
	return pullRequest{config: config, deleter: deleter}
}

func (p pullRequest) accept(url string) bool {
	return len(regexpPullRequest.FindStringSubmatch(url)) == 6
}

func (p pullRequest) buildProject(githubURL string) (project.Project, error) {
	result := regexpPullRequest.FindStringSubmatch(githubURL)

	login := result[1]
	token := result[2]
	typeHost := "github.com"
	organisation := result[3]
	projectName := result[4]
	reprocessDistantID := result[5]

	loggerPullRequest.WithFields(logrus.Fields{
		"githubURL":          githubURL,
		"organisation":       organisation,
		"projectName":        projectName,
		"reprocessDistantID": reprocessDistantID,
	}).Info("New pull request")

	project := project.Project{
		CreatedDate:        time.Now(),
		LastUpdatedDate:    time.Now(),
		Name:               projectName,
		GitURL:             githubURL,
		PathToWrite:        p.config.PathToWrite,
		BranchName:         p.config.BranchName,
		Login:              login,
		Token:              token,
		TypeHost:           typeHost,
		Organisation:       organisation,
		ReProcessDistantID: reprocessDistantID,
	}

	urlStr := fmt.Sprintf("https://api.github.com/repos/%s/%s/pulls/%s", organisation, projectName, reprocessDistantID)
	jsonRes, err := project.HTTP().Get(urlStr)
	if err != nil {
		loggerPullRequest.WithError(err).Error("Can't get pull-request details")
		return project, err
	}
	objectRes := make(map[string]interface{})
	err = json.Unmarshal(jsonRes, &objectRes)
	if err != nil {
		loggerPullRequest.WithError(err).Error("Can't unmarshal pull-request details")
		return project, err
	}

	if objectRes["state"].(string) != "close" {
		err := p.deleter.Delete(project)
		if err != nil {
			loggerPullRequest.WithError(err).Error("Can't delete project")
			return project, err
		}
		return project, errors.New("Project is deleted by pull-request closed")
	}
	if objectRes["state"].(string) != "open" {
		project = project.SetReProcessDistantID("")
	}

	bodyDetails := objectRes["body"].(string)
	pkgsToExcludeByFile := parseBody(bodyDetails)
	return project.SetExcludes(pkgsToExcludeByFile), nil
}

func parseBody(bodyDetails string) map[string][]string {
	bodyLines := strings.Split(bodyDetails, "\n")
	lastFile := ""
	pkgToExclude := make(map[string][]string)
	for _, bodyLine := range bodyLines {
		if strings.HasPrefix(bodyLine, "* [") {
			lastFile = strings.Split(bodyLine, "]")[1]
			lastFile = strings.Split(lastFile, ":")[0]
			lastFile = strings.TrimSpace(lastFile)
			if strings.HasPrefix(bodyLine, "* [ ] ") {
				pkgToExclude[lastFile] = make([]string, 0)
			}
		} else if strings.HasPrefix(bodyLine, "    * [ ]") {
			if res, ok := pkgToExclude[lastFile]; ok && len(res) == 0 {
				continue // if file is exclude no need to parse depth
			}
			if _, ok := pkgToExclude[lastFile]; !ok {
				pkgToExclude[lastFile] = make([]string, 0)
			}
			pkgNameCleaned := strings.Split(bodyLine, "]")[1]
			pkgNameCleaned = strings.Split(pkgNameCleaned, ":")[0]
			pkgNameCleaned = strings.TrimSpace(pkgNameCleaned)
			pkgToExclude[lastFile] = append(pkgToExclude[lastFile], pkgNameCleaned)
		}

	}

	return pkgToExclude
}

func (p pullRequest) download(project project.Project) (project.Project, error) {
	project = project.
		SetBranchAlreadyExist(true).
		SetGitURL(fmt.Sprintf("https://%s:%s@github.com/%s/%s.git", project.Login, project.Token, project.Organisation, project.Name))

	if err := project.Git().Clone(); err != nil {
		loggerPullRequest.Error(err)
		return project, err
	}

	return project, nil
}
