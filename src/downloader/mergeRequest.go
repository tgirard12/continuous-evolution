package downloader

import (
	"continuous-evolution/src/project"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"time"

	"github.com/sirupsen/logrus"
)

//MergeRequestConfig is configuration required by merge-request downloader
type MergeRequestConfig struct {
	Enabled bool
}

//DefaultMergeRequestConfig is the default configuration required by merge-request downloader
var DefaultMergeRequestConfig = MergeRequestConfig{Enabled: true}

var loggerMergeRequest = logrus.WithField("logger", "downloader/mergeRequest")
var regexpMergeRequest = regexp.MustCompile(`https://([^:]+):([^@]+)@([^/]+)/api/v4/projects/([^/]+)/([^/]+)/merge_requests/(.+)`)

type mergeRequest struct {
	config  Config
	deleter DeleteProject
}

func newMergeRequest(config Config, deleter DeleteProject) downloader {
	return mergeRequest{config: config, deleter: deleter}
}

func (p mergeRequest) accept(url string) bool {
	return len(regexpMergeRequest.FindStringSubmatch(url)) == 7
}

func (p mergeRequest) buildProject(gitlabURL string) (project.Project, error) {
	result := regexpMergeRequest.FindStringSubmatch(gitlabURL)

	login := result[1]
	token := result[2]
	typeHost := result[3]
	organisation := result[4]
	projectName := result[5]
	reprocessDistantID := result[6]

	loggerPullRequest.WithFields(logrus.Fields{
		"gitlabURL":          gitlabURL,
		"organisation":       organisation,
		"projectName":        projectName,
		"reprocessDistantID": reprocessDistantID,
	}).Info("New merge request")

	project := project.Project{
		CreatedDate:        time.Now(),
		LastUpdatedDate:    time.Now(),
		Name:               projectName,
		GitURL:             gitlabURL,
		PathToWrite:        p.config.PathToWrite,
		BranchName:         p.config.BranchName,
		Login:              login,
		Token:              token,
		TypeHost:           typeHost,
		Organisation:       organisation,
		ReProcessDistantID: reprocessDistantID,
	}

	urlStr := fmt.Sprintf("https://%s/api/v4/projects/%s/merge_requests/%s?private_token=%s", typeHost, url.PathEscape(organisation+"/"+projectName), reprocessDistantID, token)
	jsonRes, err := project.HTTP().Get(urlStr)
	if err != nil {
		loggerMergeRequest.WithError(err).Error("Can't get merge-request details")
		return project, err
	}
	objectRes := make(map[string]interface{})
	err = json.Unmarshal(jsonRes, &objectRes)
	if err != nil {
		loggerMergeRequest.WithError(err).Error("Can't unmarshal merge-request details")
		return project, err
	}

	if objectRes["state"].(string) == "closed" {
		err := p.deleter.Delete(project)
		if err != nil {
			loggerMergeRequest.WithError(err).Error("Can't delete project")
			return project, err
		}
		return project, errors.New("Project is deleted by merge-request closed")
	}
	if objectRes["state"].(string) != "opened" {
		project = project.SetReProcessDistantID("")
	}

	bodyDetails := objectRes["description"].(string)
	pkgsToExcludeByFile := parseBody(bodyDetails)
	return project.SetExcludes(pkgsToExcludeByFile), nil
}

func (p mergeRequest) download(project project.Project) (project.Project, error) {
	project = project.
		SetBranchAlreadyExist(true).
		SetGitURL(fmt.Sprintf("https://%s:%s@%s/%s/%s.git", project.Login, project.Token, project.TypeHost, project.Organisation, project.Name))

	if err := project.Git().Clone(); err != nil {
		loggerMergeRequest.Error(err)
		return project, err
	}

	return project, nil
}
