package downloader

import (
	"testing"
)

const data = `New dependencies versions :

* [x] pakage/Dockerfile :
    * [x] depthName : 1.0.0
    * [x] other : 0.0.1 => 0.2.0

* [X] package/notExcluded
    * noExcludes

* [x] pakage/pom.xml :
    * [x] depthName1 : 0.1.0
    * [ ] depthName2 : 3.0.0
    * [ ] depthName3 : 4.1.0

* [ ] pakage/noDepth

[Relaunch update](http://test.com)

> This merge request is generate, no human has tested the resulting code. If you are confident with your CI accept it, else fetch the result and test it manually. By the way adding CI is always a good idea ;)

Gracefully updated by [ContinuousEvolution](https://gitlab.com/ContinuousEvolution/continuous-evolution) with :heart:`

func TestParsing(t *testing.T) {
	pkgToExclude := parseBody(data)
	if len(pkgToExclude) != 2 {
		t.Fatalf("Should return 1 file to exclude and 1 pkg to exclude instead of %d", len(pkgToExclude))
	}
	file, ok := pkgToExclude["pakage/noDepth"]
	if !ok {
		t.Fatalf("Should exclude pakage/noDepth")
	}
	if len(file) != 0 {
		t.Fatalf("Should exclude pakage/noDepth withtout pkg because we exclude all")
	}
	pkg, ok := pkgToExclude["pakage/pom.xml"]
	if !ok {
		t.Fatalf("Should exclude depthName2 from pakage/pom.xml")
	}
	if len(pkg) != 2 {
		t.Fatalf("Should exclude pakage/pom.xml depthName2 and depthName3 instead of %s", pkg)
	}
	if pkg[0] != "depthName2" {
		t.Fatalf("Should exclude depthName2 from pakage/pom.xml instead of %s", pkg[0])
	}
	if pkg[1] != "depthName3" {
		t.Fatalf("Should exclude depthName3 from pakage/pom.xml instead of %s", pkg[1])
	}
}

const data2 = `New dependencies versions :

* [ ] pakage/Dockerfile :
    * [ ] depthName : 1.0.0
    * [x] other : 0.0.1 => 0.2.0

[Relaunch update](http://test.com)

> This merge request is generate, no human has tested the resulting code. If you are confident with your CI accept it, else fetch the result and test it manually. By the way adding CI is always a good idea ;)

Gracefully updated by [ContinuousEvolution](https://gitlab.com/ContinuousEvolution/continuous-evolution) with :heart:`

func TestParsingNoFile(t *testing.T) {
	pkgToExclude := parseBody(data2)
	if len(pkgToExclude) != 1 {
		t.Fatalf("Should return 1 file instead of %d", len(pkgToExclude))
	}
	file, ok := pkgToExclude["pakage/Dockerfile"]
	if !ok {
		t.Fatalf("Should exclude pakage/Dockerfile")
	}
	if len(file) != 0 {
		t.Fatalf("Should exclude pakage/Dockerfile withtout pkg because we exclude all")
	}
}
