package tools

import (
	"bytes"

	"github.com/fsouza/go-dockerclient"
	"github.com/sirupsen/logrus"
)

const dockerLocal = "unix:///var/run/docker.sock"

//Docker is an helper to run docker image easily
type Docker interface {
	StartDocker(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte
}

type dockerImpl struct {
	client *docker.Client
}

//NewDocker build a new Docker
func NewDocker() Docker {
	client, err := docker.NewClient(dockerLocal)
	if err != nil {
		panic(err)
	}

	return &dockerImpl{client: client}
}

func (clientImpl *dockerImpl) StartDocker(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
	logrus.WithField("image", image).WithField("cmd", cmd).WithField("binds", binds).Info("start container")
	hostConfig := docker.HostConfig{Binds: binds, DNS: dns, AutoRemove: true}
	createOpts := docker.CreateContainerOptions{
		Config: &docker.Config{
			Image:      image,
			Cmd:        cmd,
			WorkingDir: workDir,
			Env:        env,
		},
		HostConfig: &hostConfig,
	}
	clientImpl.client.PullImage(docker.PullImageOptions{Repository: image}, docker.AuthConfiguration{})
	container, err := clientImpl.client.CreateContainer(createOpts)
	if err != nil {
		logrus.WithField("createOpts", createOpts).WithError(err).Error("can't create container")
	}

	err = clientImpl.client.StartContainer(container.ID, &hostConfig)
	if err != nil {
		logrus.WithField("container", container).WithError(err).Error("can't start container")
	}

	var stdoutBuffer, stderrBuffer bytes.Buffer
	opts := docker.LogsOptions{
		Container:    container.ID,
		OutputStream: &stdoutBuffer,
		ErrorStream:  &stderrBuffer,
		Follow:       true,
		Stdout:       true,
		Stderr:       true,
		Tail:         "0",
	}

	clientImpl.client.Logs(opts)

	status, err := clientImpl.client.WaitContainer(container.ID)
	if err != nil {
		logrus.WithField("container", container).WithError(err).Error("can't wait for container")
	}
	if status != 0 {
		logrus.WithField("container", container).WithError(err).Errorf("WaitContainer(%q): wrong status. Want 0. Got %d", container.ID, status)
	}
	logrus.WithField("image", image).WithField("cmd", cmd).WithField("binds", binds).Info("finish container")
	return stdoutBuffer.Bytes()
}
