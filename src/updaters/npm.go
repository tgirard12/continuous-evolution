package updaters

import (
	"bytes"
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"encoding/json"
	"os"

	"strings"

	"github.com/sirupsen/logrus"
)

//NpmConfig is configuration required by npm updater
type NpmConfig struct {
	Enabled bool
	Image   string
	Command []string
	Volumes []string
	Env     []string
	DNS     []string
}

//DefaultNpmConfig is the default configuration required by npm updater
var DefaultNpmConfig = NpmConfig{
	Enabled: true,
	Image:   "creack/ncu",
	Command: []string{"--upgrade", "--upgradeAll", "--packageFile", "/app/package.json"},
	Volumes: []string{},
	Env:     []string{},
	DNS:     []string{},
}

var npmLogger = logrus.WithField("logger", "updaters/npm")

func npmIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "package.json"
}

type npmUpdater struct {
	config       NpmConfig
	dockerClient tools.Docker
}

func newNpm(config Config) Updater {
	return &npmUpdater{config: config.Npm, dockerClient: tools.NewDocker()}
}

func (u *npmUpdater) Update(_ string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	newDepth, err := u.getDepthUpdated(pkg, excludes)
	if err != nil {
		return nil, err
	}

	if len(newDepth) > 0 {
		binds := append(u.config.Volumes, pkg.Path+":/app/package.json")
		cmd := u.config.Command
		if len(excludes) > 0 {
			cmd = append(cmd, "--reject", strings.Join(excludes, ","))
		}
		u.dockerClient.StartDocker(u.config.Image, cmd, binds, "/tmp", u.config.Env, u.config.DNS)
	}

	return newDepth, nil
}

func (u *npmUpdater) getDepthUpdated(pkg project.Package, excludes []string) ([]project.Dependency, error) {
	binds := append(u.config.Volumes, pkg.Path+":/app/package.json")
	cmd := []string{"--upgrade", "--upgradeAll", "--jsonUpgraded", "--packageFile", "/app/package.json"}
	if len(excludes) > 0 {
		cmd = append(cmd, "--reject", strings.Join(excludes, ","))
	}
	logs := u.dockerClient.StartDocker(u.config.Image, cmd, binds, "/tmp", u.config.Env, u.config.DNS)
	npmLogger.WithField("logs", string(logs)).Info("Logs during getDepthUpdated")
	splitedLogs := bytes.Split(logs, []byte("\n"))
	if len(splitedLogs) < 3 {
		return make([]project.Dependency, 0), nil
	}
	goodLog := splitedLogs[len(splitedLogs)-2]
	jsonDeserialized := make(map[string]string)
	err := json.Unmarshal(goodLog, &jsonDeserialized)
	if err != nil {
		return nil, err
	}
	var dependencies []project.Dependency
	for key, value := range jsonDeserialized {
		dependencies = append(dependencies, project.Dependency{Name: key, NewVersion: value, CanBeExcludes: true})
	}
	npmLogger.WithField("newDepth", dependencies).Info("New depth found")
	return dependencies, nil
}
