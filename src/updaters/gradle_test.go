package updaters

import (
	"os"
	"testing"
	"time"
)

type fakeFileInfo struct {
	name  string
	isDir bool
}

func (f fakeFileInfo) Name() string     { return f.name }
func (fakeFileInfo) Size() int64        { return 0 }
func (fakeFileInfo) Mode() os.FileMode  { return os.ModePerm }
func (fakeFileInfo) ModTime() time.Time { return time.Now() }
func (f fakeFileInfo) IsDir() bool      { return f.isDir }
func (fakeFileInfo) Sys() interface{}   { return nil }

func TestGradleIsPackageFile(t *testing.T) {
	res := gradleIsPackageFile("settings.gradle", fakeFileInfo{name: "settings.gradle", isDir: false})
	if !res {
		t.Fatal("gradle updater should return true for settings.gradle")
	}
	res = gradleIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("gradle updater should return false for other")
	}
	res = gradleIsPackageFile("settings.gradle", fakeFileInfo{name: "settings.gradle", isDir: true})
	if res {
		t.Fatal("gradle updater should return false for directory even if its name is settings.gradle")
	}
}

var originalBuildGradle = `
buildscript {
    apply from: "${rootDir}/gradle/dependencies-version.gradle"

    repositories {
        maven { url "https://repo.spring.io/snapshot" }
        maven { url "https://repo.spring.io/milestone" }
    }

    dependencies {
        // spring
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${plugin_springboot_version}")

        // junit
        classpath("org.junit.platform:junit-platform-gradle-plugin:${plugin_junit_version}")

        // swagger
        classpath("io.swagger:swagger-codegen:${plugin_swagger_version}")
    }
}

allprojects { projects ->
    group group
    version version

    apply plugin: 'java'
    sourceCompatibility = 1.8
    targetCompatibility = 1.8

    apply from: "${rootDir}/gradle/dependencies-version.gradle"

    repositories {
        maven { url "https://repo.spring.io/snapshot" }
        maven { url "https://repo.spring.io/milestone" }
    }


}

subprojects {
    dependencies {
        compile "org.slf4j:slf4j-api:${slf4j_version}"
    }

}
`

var wantedBuildGradle = `
buildscript {
    apply from: "${rootDir}/gradle/dependencies-version.gradle"

    repositories {
        maven { url "https://repo.spring.io/snapshot" }
        maven { url "https://repo.spring.io/milestone" }
    }

    dependencies {
        // spring
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${plugin_springboot_version}")

        // junit
        classpath("org.junit.platform:junit-platform-gradle-plugin:${plugin_junit_version}")

        // swagger
        classpath("io.swagger:swagger-codegen:${plugin_swagger_version}")
    }
}

plugins {
    id 'com.github.ben-manes.versions' version '0.17.0'
}

allprojects { projects ->
    group group
    version version

    apply plugin: 'java'
    sourceCompatibility = 1.8
    targetCompatibility = 1.8

    apply from: "${rootDir}/gradle/dependencies-version.gradle"

    repositories {
        maven { url "https://repo.spring.io/snapshot" }
        maven { url "https://repo.spring.io/milestone" }
    }


}

subprojects {
    dependencies {
        compile "org.slf4j:slf4j-api:${slf4j_version}"
    }

}
`

func TestUpdateBuildGradleWithPlugin(t *testing.T) {
	result := updateBuildGradleWithPlugin(originalBuildGradle)
	if result != wantedBuildGradle {
		println(result)
		t.Fatal("Bad plugin insertion")
	}
}

var dependenciesForms = `
    compile group: 'org.hibernate', name: 'hibernate-core', version: '3.6.7.Final'
    compile 'org.hibernate:hibernate-core:3.6.7.Final'
    testCompile "org.dbunit:dbunit:${dbunitVersion}"
    compile("org.dbunit:dbunit:${dbunitVersion}") {
        exclude group: 'org.flywaydb' 
    }
`

var dependenciesWanted = `
    compile group: 'org.hibernate', name: 'hibernate-core', version: '12'
    compile group: 'org.hibernate', name: 'hibernate-core', version: '12'
    testCompile group: 'org.dbunit', name: 'dbunit', version: '1.0.0'
    compile("org.dbunit:dbunit:1.0.0") {
        exclude group: 'org.flywaydb' 
    }
`

func TestUpdateBuildGradleWithNewVersion(t *testing.T) {
	result, _ := updateBuildGradleWithNewVersion(dependenciesForms, []dependency{
		{
			Group:     "org.hibernate",
			Version:   "3.6.7.Final",
			Name:      "hibernate-core",
			Available: dependencyAvailable{Milestone: "12"},
		},
		{
			Group:     "org.dbunit",
			Version:   "0.0.5",
			Name:      "dbunit",
			Available: dependencyAvailable{Milestone: "1.0.0"},
		},
	})
	if result != dependenciesWanted {
		println("------------")
		println(result)
		println("------------")
		t.Fatal("Bad plugin insertion")
	}
}
