package updaters

import (
	"bytes"
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/sergi/go-diff/diffmatchpatch"
)

const logs = `[info] Loading project definition from /tmp/project
[info] Updating ProjectRef(uri("file:/tmp/project/"), "tmp-build")...
[info] Done updating.
[info] Loading settings from build.sbt ...
[info] Set current project to cats-sandbox (in build file:/tmp/)
[info] Found 2 dependency updates for cats-sandbox
[info]   org.spire-math:kind-projector:plugin->default(compile) : 0.9.3 -> 0.9.6
[info]   org.typelevel:cats-core                                : 1.0.0 -> 1.0.1`

func TestSbtIdpAckageFile(t *testing.T) {
	res := sbtIsPackageFile("build.sbt", fakeFileInfo{name: "build.sbt", isDir: false})
	if !res {
		t.Fatal("sbt updater should return true for build.sbt")
	}
	res = sbtIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("sbt updater should return false for other")
	}
	res = sbtIsPackageFile("build.sbt", fakeFileInfo{name: "build.sbt", isDir: true})
	if res {
		t.Fatal("sbt updater should return false for directory even if its name is build.sbt")
	}
}

func TestSbtFoundOutdatedDependencies(t *testing.T) {
	sbt := &sbtUpdater{config: DefaultConfig.Sbt, dockerClient: tools.NewDocker()}
	dep, err := sbt.findOutdatedDependencies("cats-sandbox", []byte(logs))
	if err != nil {
		t.Fatal("Valid test should not throw err")
	}
	if len(dep) != 2 {
		t.Fatalf("Should return 2 dep instead of %d", len(dep))
	}
	if dep[0].Name != "org.spire-math:kind-projector:plugin->default(compile)" {
		t.Fatalf("Should return good name instead of %s", dep[0].Name)
	}
	if dep[0].OriginalVersion != "0.9.3" {
		t.Fatalf("Should return good original version instead of %s", dep[0].OriginalVersion)
	}
	if dep[0].NewVersion != "0.9.6" {
		t.Fatalf("Should return good new version instead of %s", dep[0].NewVersion)
	}
}

const logsTransitive = `[info]  [SUCCESSFUL ] com.fasterxml.jackson.core#jackson-annotations;2.8.0!jackson-annotations.jar(bundle) (73ms)
[info] downloading https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.8.8/jackson-core-2.8.8.jar ...
[info]  [SUCCESSFUL ] com.fasterxml.jackson.core#jackson-core;2.8.8!jackson-core.jar(bundle) (103ms)
[info] Done updating.
[info] Set current project to dotty-simple (in build file:/tmp/)
[info] Found 1 dependency update for dotty-simple
[info]   ch.epfl.lamp:scala-library : 0.7.0-RC1 -> 0.7.0-bin-20180304-4cfffbe-NIGHTLY -> 0.8.0-bin-20180308-cfef000-NIGHTLY`

func TestSbtFoundOutdatedDependenciesWithTransitive(t *testing.T) {
	sbt := &sbtUpdater{config: DefaultConfig.Sbt, dockerClient: tools.NewDocker()}
	dep, err := sbt.findOutdatedDependencies("cats-sandbox", []byte(logsTransitive))
	if err != nil {
		t.Fatal("Valid test should not throw err")
	}
	if len(dep) != 0 {
		t.Fatalf("Should return 0 dep because of transitive dep instead of %d", len(dep))
	}
}

const file = `name := "cats-sandbox"
version := "0.0.1-SNAPSHOT"

scalaVersion := "2.12.4"

scalacOptions ++= Seq(
  "-encoding", "UTF-8",   // source files are in UTF-8
  "-deprecation",         // warn about use of deprecated APIs
  "-unchecked",           // warn about unchecked type parameters
  "-feature",             // warn about misused language features
  "-Xlint",               // enable handy linter warnings
  "-Xfatal-warnings",     // turn compiler warnings into errors
  "-Ypartial-unification" // allow the compiler to unify type constructors of different arities
)

libraryDependencies += "org.typelevel" %% "cats-core" % "1.0.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.3")`

const updatedFile = `name := "cats-sandbox"
version := "0.0.1-SNAPSHOT"

scalaVersion := "2.12.4"

scalacOptions ++= Seq(
  "-encoding", "UTF-8",   // source files are in UTF-8
  "-deprecation",         // warn about use of deprecated APIs
  "-unchecked",           // warn about unchecked type parameters
  "-feature",             // warn about misused language features
  "-Xlint",               // enable handy linter warnings
  "-Xfatal-warnings",     // turn compiler warnings into errors
  "-Ypartial-unification" // allow the compiler to unify type constructors of different arities
)

libraryDependencies += "org.typelevel" %% "cats-core" % "1.0.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.6")`

func TestSbtUpdateFile(t *testing.T) {
	err := os.MkdirAll("/tmp/sbt/testUpdateFile", os.ModePerm)
	if err != nil {
		t.Fatal("Can't create dir /tmp/sbt/testUpdateFile", err)
	}
	err = ioutil.WriteFile("/tmp/sbt/testUpdateFile/build.sbt", []byte(file), os.ModePerm)
	if err != nil {
		t.Fatal("Can't write file /tmp/sbt/testUpdateFile/build.sbt", err)
	}

	sbt := &sbtUpdater{config: DefaultConfig.Sbt, dockerClient: tools.NewDocker()}
	err = sbt.updateFile("/tmp/sbt/testUpdateFile/", []project.Dependency{
		{Name: "org.typelevel:cats-core", OriginalVersion: "1.0.0", NewVersion: "1.0.1"},
		{Name: "org.spire-math:kind-projector:plugin->default(compile)", OriginalVersion: "0.9.3", NewVersion: "0.9.6"},
	})
	if err != nil {
		t.Fatal("valid test should not return error", err)
	}

	data, err := ioutil.ReadFile("/tmp/sbt/testUpdateFile/build.sbt")
	if err != nil {
		t.Fatal("Can't read file /tmp/sbt/testUpdateFile/build.sbt", err)
	}
	if bytes.Compare(data, []byte(updatedFile)) != 0 {
		dmp := diffmatchpatch.New()
		diffs := dmp.DiffMain(updatedFile, string(data), true)
		fmt.Println(dmp.DiffPrettyText(diffs))
		t.Fatal("diff found in sbt")
	}

	err = os.RemoveAll("/tmp/sbt/testUpdateFile")
	if err != nil {
		t.Fatal("Can't delete /tmp/sbt/testUpdateFile", err)
	}
}

func TestRemoveExcludes(t *testing.T) {
	sbt := &sbtUpdater{config: DefaultConfig.Sbt, dockerClient: tools.NewDocker()}
	dep := sbt.removeExcludes([]project.Dependency{{Name: "org.typelevel:cats-core"}, {Name: "ch.epfl.lamp:scala-library"}}, []string{"ch.epfl.lamp:scala-library"})
	if len(dep) != 1 {
		t.Fatalf("dep should have %d len instead of %d after remove excludes", 1, len(dep))
	}
}
