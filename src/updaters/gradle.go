package updaters

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

//GradleConfig is configuration required by gradle updater
type GradleConfig struct {
	Enabled bool
	Image   string
	Command []string
	Volumes []string
	Env     []string
	DNS     []string
}

//DefaultGradleConfig is the default configuration required by gradle updater
var DefaultGradleConfig = GradleConfig{
	Enabled: true,
	Image:   "gradle:4.3-jre8",
	Command: []string{"gradle", "dependencyUpdate", "-DoutputFormatter=json", "--stacktrace"},
	Volumes: []string{},
	Env:     []string{},
	DNS:     []string{},
}

var loggerGradle = logrus.WithField("logger", "updaters/gradle")

func gradleIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "settings.gradle"
}

type gradle struct {
	config       GradleConfig
	alreadyFound bool
	dockerClient tools.Docker
}

func newGradle(config Config) Updater {
	return &gradle{config: config.Gradle, dockerClient: tools.NewDocker(), alreadyFound: false}
}

type report struct {
	Outdated dependencies
}

type dependencies struct {
	Dependencies []dependency
}

type dependency struct {
	Group     string
	Version   string
	Name      string
	Available dependencyAvailable
}

type dependencyAvailable struct {
	Release     string
	Milestone   string
	Integration string
}

func (u *gradle) Update(projectPath string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	gradleData, err := ioutil.ReadFile(projectPath + "/build.gradle")
	if err != nil {
		return nil, err
	}
	newGradle := updateBuildGradleWithPlugin(string(gradleData))
	if err := ioutil.WriteFile(projectPath+"/build.gradle", []byte(newGradle), 0666); err != nil {
		return nil, err
	}

	binds := append(u.config.Volumes, projectPath+":/tmp")
	u.dockerClient.StartDocker(u.config.Image, u.config.Command, binds, "/tmp", u.config.Env, u.config.DNS)

	if err := ioutil.WriteFile(projectPath+"/build.gradle", []byte(gradleData), 0666); err != nil {
		return nil, err
	}

	files, err := ioutil.ReadDir(projectPath)
	if err != nil {
		return nil, err
	}

	outdatedDependencies, err := findOutdatedDependencies(projectPath, files)
	if err != nil {
		return nil, err
	}

	outdatedDependenciesWithoutExcludes := make([]dependency, 0)
	for _, dep := range outdatedDependencies {
		found := false
		for _, exclude := range excludes {
			if dep.Group+"."+dep.Name == exclude {
				found = true
				break
			}
		}
		if !found {
			outdatedDependenciesWithoutExcludes = append(outdatedDependenciesWithoutExcludes, dep)
		}
	}

	if len(outdatedDependenciesWithoutExcludes) == 0 {
		return nil, nil
	}

	updatedDepencies := make([]project.Dependency, 0)
	newDep, err := updateFile(projectPath, outdatedDependenciesWithoutExcludes)
	if err != nil {
		return nil, err
	}
	updatedDepencies = append(updatedDepencies, newDep...)

	for _, f := range files {
		if f.IsDir() {
			newDep, err := updateFile(projectPath+"/"+f.Name(), outdatedDependenciesWithoutExcludes)
			if err != nil {
				return nil, err
			}
			updatedDepencies = append(updatedDepencies, newDep...)
		}
	}

	return updatedDepencies, nil
}

func findOutdatedDependencies(projectPath string, files []os.FileInfo) ([]dependency, error) {
	outdatedDependencies := make([]dependency, 0)
	fatherReport, err := findReport(projectPath)
	if err != nil {
		return nil, err
	}
	for _, dep := range fatherReport.Outdated.Dependencies {
		outdatedDependencies = append(outdatedDependencies, dep)
	}

	for _, f := range files {
		if f.IsDir() {
			report, err := findReport(projectPath + "/" + f.Name())
			if err != nil {
				return nil, err
			}
			for _, dep := range report.Outdated.Dependencies {
				outdatedDependencies = append(outdatedDependencies, dep)
			}
		}
	}
	return outdatedDependencies, nil
}

func findReport(pathModule string) (report, error) {
	var report report
	_, err := os.Stat(pathModule + "/build/dependencyUpdates/report.json")
	if os.IsNotExist(err) {
		return report, nil
	} else if err != nil {
		return report, err
	}
	raw, err := ioutil.ReadFile(pathModule + "/build/dependencyUpdates/report.json")
	if err != nil {
		loggerGradle.WithError(err).Error("Can't read report file")
		return report, err
	}
	err = json.Unmarshal(raw, &report)
	if err != nil {
		loggerGradle.WithError(err).Error("Can't unmarshal report json")
		return report, err
	}
	loggerGradle.WithField("report", report).Warn("Found report")
	return report, nil
}

func updateFile(pathModule string, outdatedDependencies []dependency) ([]project.Dependency, error) {
	_, err := os.Stat(pathModule + "/build.gradle")
	if os.IsNotExist(err) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	originalGradle, err := ioutil.ReadFile(pathModule + "/build.gradle")
	if err != nil {
		loggerGradle.WithError(err).Error("Can't read build.gradle file")
		return nil, err
	}
	newGradleData, updatedDepencies := updateBuildGradleWithNewVersion(string(originalGradle), outdatedDependencies)
	if err := ioutil.WriteFile(pathModule+"/build.gradle", []byte(newGradleData), 0666); err != nil {
		return nil, err
	}

	loggerGradle.WithField("outdatedDependencies", outdatedDependencies).WithField("module", pathModule).Warn("found update")
	return updatedDepencies, nil
}

func updateBuildGradleWithPlugin(originalContent string) string {
	lines := strings.Split(originalContent, "\n")
	resultLines := make([]string, 0, len(lines))
	found := false
	for _, line := range lines {
		resultLines = append(resultLines, line)
		if !found && strings.HasPrefix(line, "}") {
			found = true
			resultLines = append(resultLines, `
plugins {
    id 'com.github.ben-manes.versions' version '0.17.0'
}`)
		}
	}
	return strings.Join(resultLines, "\n")
}

func updateBuildGradleWithNewVersion(originalContent string, outdatedDependencies []dependency) (string, []project.Dependency) {
	lines := strings.Split(originalContent, "\n")
	resultLines := make([]string, 0, len(lines))
	updatedDependencies := make([]project.Dependency, 0)

	for _, l := range lines {
		for _, dependency := range outdatedDependencies {
			if strings.Contains(l, dependency.Group) && strings.Contains(l, dependency.Name) {
				updatedDependencies = append(updatedDependencies, project.Dependency{
					Name:            dependency.Group + "." + dependency.Name,
					OriginalVersion: dependency.Version,
					NewVersion:      dependency.Available.Milestone,
					CanBeExcludes:   true,
				})
				spacePrefix := leadingSpace(l)
				scope := strings.Split(strings.TrimSpace(l), " ")[0]
				if strings.Contains(l, "(") {
					scope = strings.Split(strings.TrimSpace(l), "(")[0]
				}
				if strings.HasSuffix(l, "{") {
					l = spacePrefix + scope + `("` + dependency.Group + ":" + dependency.Name + ":" + dependency.Available.Milestone + `") {`
				} else {
					l = spacePrefix + scope + " group: '" + dependency.Group + "', name: '" + dependency.Name + "', version: '" + dependency.Available.Milestone + "'"
				}
				break
			}
		}
		resultLines = append(resultLines, l)
	}
	loggerGradle.WithField("original", originalContent).WithField("outdatedDependencies", outdatedDependencies).WithField("new content", resultLines).Warn("Gradle updated")
	return strings.Join(resultLines, "\n"), updatedDependencies
}

func leadingSpace(line string) string {
	res := ""
	for _, runeValue := range line {
		if runeValue == ' ' {
			res += " "
		} else if runeValue == '	' {
			res += "	"
		} else {
			break
		}
	}
	return res
}
