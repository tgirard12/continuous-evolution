package updaters

import "testing"

func TestMvnIsPackageFile(t *testing.T) {
	res := mvnIsPackageFile("pom.xml", fakeFileInfo{name: "pom.xml", isDir: false})
	if !res {
		t.Fatal("mvn updater should return true for pom.xml")
	}
	res = mvnIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("mvn updater should return false for other")
	}
	res = mvnIsPackageFile("pom.xml", fakeFileInfo{name: "pom.xml", isDir: true})
	if res {
		t.Fatal("mvn updater should return false for directory even if its name is pom.xml")
	}
}
