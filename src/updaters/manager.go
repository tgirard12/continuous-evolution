package updaters

import (
	"continuous-evolution/src/project"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"strings"

	"github.com/sirupsen/logrus"
)

//Config is configuration required by updater
type Config struct {
	PoolSize      int
	Parallel      int
	Docker        DockerConfig
	Dockercompose DockerComposeConfig
	Golangdep     GolangDepConfig
	Gradle        GradleConfig
	Maven         MavenConfig
	Npm           NpmConfig
	Pip           PipConfig
	Sbt           SbtConfig
}

//DefaultConfig is the default configuration required by updater
var DefaultConfig = Config{PoolSize: 1, Parallel: 1, Docker: DefaultDockerConfig, Dockercompose: DefaultDockerComposeConfig, Golangdep: DefaultGolangDepConfig,
	Gradle: DefaultGradleConfig, Maven: DefaultMavenConfig, Npm: DefaultNpmConfig, Pip: DefaultPipConfig, Sbt: DefaultSbtConfig}

var managerLogger = logrus.WithField("logger", "updaters/manager")

//Updater detect file can be updated and update it
type Updater interface {
	Update(projectPath string, pkg project.Package, excludes []string) ([]project.Dependency, error)
}

type internalUpdater struct {
	IsPackageFile func(pathString string, f os.FileInfo) bool
	Updater       func(Config) Updater
}

type manager struct {
	config   Config
	updaters map[project.PackageType]internalUpdater
}

//Manager is reposible to download the code
type Manager interface {
	PackageFromFile(project project.Project) (project.Project, error)
	Update(project project.Project) (project.Project, error)
}

//NewManager return a Manager configured
func NewManager(config Config) Manager {
	updaters := make(map[project.PackageType]internalUpdater)
	if config.Npm.Enabled {
		updaters[project.PackageType("npm")] = internalUpdater{IsPackageFile: npmIsPackageFile, Updater: newNpm}
	}
	if config.Maven.Enabled {
		updaters[project.PackageType("mvn")] = internalUpdater{IsPackageFile: mvnIsPackageFile, Updater: newMvn}
	}
	if config.Pip.Enabled {
		updaters[project.PackageType("pip")] = internalUpdater{IsPackageFile: pipIsPackageFile, Updater: newPip}
	}
	if config.Docker.Enabled {
		updaters[project.PackageType("docker")] = internalUpdater{IsPackageFile: dockerIsPackageFile, Updater: newDocker}
	}
	if config.Dockercompose.Enabled {
		updaters[project.PackageType("docker-compose")] = internalUpdater{IsPackageFile: dockerComposeIsPackageFile, Updater: newDockerCompose}
	}
	if config.Golangdep.Enabled {
		updaters[project.PackageType("golangDep")] = internalUpdater{IsPackageFile: golangDepIsPackageFile, Updater: newGolangDep}
	}
	if config.Gradle.Enabled {
		updaters[project.PackageType("gradle")] = internalUpdater{IsPackageFile: gradleIsPackageFile, Updater: newGradle}
	}
	if config.Sbt.Enabled {
		updaters[project.PackageType("sbt")] = internalUpdater{IsPackageFile: sbtIsPackageFile, Updater: newSbt}
	}

	return &manager{
		config:   config,
		updaters: updaters,
	}
}

//PackageFromFile visit all project.Project files to find package to update. Add them to a new project.project and return it.
func (m *manager) PackageFromFile(p project.Project) (project.Project, error) {
	packages := make([]project.Package, 0)

	root := fmt.Sprintf("%s/%s", p.PathToWrite, p.Name)
	lastNoGitDirs := "/impossibleDir" //Work because filepath.Walk iterates over dir
	visit := func(pathString string, f os.FileInfo, err error) error {
		if err != nil {
			managerLogger.WithField("path", pathString).WithError(err).Error("Can't visit file")
			return nil //return nil to continue visit
		}
		if !strings.HasPrefix(pathString, lastNoGitDirs) {
			isGitTracked, err := p.Git().TrackFile(pathString)
			if err != nil {
				managerLogger.WithField("path", pathString).WithError(err).Error("Can't get if file is git tracked")
				return nil //return nil to continue visit
			}
			if !isGitTracked && f.IsDir() {
				lastNoGitDirs = pathString + "/"
			}
			for updaterType, updater := range m.updaters {
				if isGitTracked && updater.IsPackageFile(pathString, f) {
					packages = append(packages, project.Package{Type: updaterType, Path: pathString})
				}
			}
		}
		return nil //return nil to continue visit
	}

	if err := filepath.Walk(root, visit); err != nil {
		return p, err
	}

	managerLogger.WithField("project", p.GitURL).WithField("pkg", packages).WithField("path", root).Info("new packages found")
	return *p.AddPackages(packages), nil
}

//Update call all updater with project if project.Project.Packages.Type match updater.Type
func (m *manager) Update(p project.Project) (project.Project, error) {
	copy := p.Copy()
	copy.Packages = make([]project.Package, 0)

	managerLogger.WithField("excludes", p.Excludes).Info("Start update project")
	muCopy := sync.Mutex{}
	waitUpdaters := sync.WaitGroup{}
	nbParallel := 0

	for updaterType, updater := range m.updaters {
		updaterInstance := updater.Updater(m.config)
		for _, pkg := range p.Packages {
			if pkg.Type == updaterType {
				excludes, isExcludeFile := p.Excludes[strings.TrimPrefix(pkg.Path, "/tmp/"+p.Name+"/")]
				managerLogger.WithField("updater", pkg.Type).WithField("excludes", excludes).Info("Start update")
				if isExcludeFile && len(excludes) == 0 {
					muCopy.Lock()
					copy = copy.AddPackages([]project.Package{pkg})
					muCopy.Unlock()
					continue //we exclude the file if no depth
				} else {
					if !isExcludeFile {
						excludes = []string{}
					}
					waitUpdaters.Add(1)
					nbParallel++
					go func(p project.Project, pkg project.Package, excludes []string) {
						root := fmt.Sprintf("%s/%s", p.PathToWrite, p.Name)
						newDepth, err := updaterInstance.Update(root, pkg, excludes)
						if err != nil {
							managerLogger.WithField("pkg", pkg).WithError(err).Error("Error during update")
						} else {
							newPkg := pkg.AddUpdatedDependencies(newDepth)
							muCopy.Lock()
							copy = copy.AddPackages([]project.Package{newPkg})
							muCopy.Unlock()
						}
						waitUpdaters.Done()
					}(p, pkg, excludes)
				}
			}
			if nbParallel == m.config.Parallel {
				waitUpdaters.Wait()
				nbParallel = 0
				waitUpdaters = sync.WaitGroup{}
			}
		}
	}

	waitUpdaters.Wait()
	return *copy, nil
}
