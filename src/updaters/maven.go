package updaters

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"os"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
)

//MavenConfig is configuration required by maven updater
type MavenConfig struct {
	Enabled bool
	Image   string
	Command []string
	Volumes []string
	Env     []string
	DNS     []string
}

//DefaultMavenConfig is the default configuration required by maven updater
var DefaultMavenConfig = MavenConfig{
	Enabled: true,
	Image:   "maven:3.5.0",
	Command: []string{"mvn", "versions:use-latest-versions", "-DgenerateBackupPoms=false"},
	Volumes: []string{},
	Env:     []string{},
	DNS:     []string{},
}

var loggerMvn = logrus.WithField("logger", "updaters/maven")

func mvnIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "pom.xml"
}

type mavenUpdater struct {
	config       MavenConfig
	alreadyFound bool
	dockerClient tools.Docker
}

func newMvn(config Config) Updater {
	return &mavenUpdater{config: config.Maven, dockerClient: tools.NewDocker(), alreadyFound: false}
}

func (u *mavenUpdater) Update(projectPath string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	dirpath := path.Dir(pkg.Path)
	isRoot := projectPath == dirpath
	binds := append(u.config.Volumes, projectPath+":/tmp")
	workdir := "/tmp"
	cmd := u.config.Command
	if !isRoot {
		module := strings.TrimPrefix(dirpath, projectPath)
		workdir = workdir + module
	} else {
		cmd = append(cmd, "--non-recursive")
	}
	if len(excludes) > 0 {
		cmd = append(cmd, "-Dexcludes="+strings.Join(excludes, ","))
	}
	loggerMvn.WithField("cmd", cmd).WithField("workdir", workdir).WithField("projectPath", projectPath).Warn("Execute maven")
	u.dockerClient.StartDocker(u.config.Image, cmd, binds, workdir, u.config.Env, u.config.DNS)
	return nil, nil
}
