package updaters

import (
	"bytes"
	"continuous-evolution/src/project"
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/sergi/go-diff/diffmatchpatch"
)

func TestFetchLatest(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		return []string{"1.0.2", "0.5", "1.0.0", "pre-2.0.0"}, nil
	}
	pkgParsed, err := d.(*dockerUpdater).parseDockerPkg("")
	if err != nil {
		t.Fatal("Valid pkg should not throw error", err)
	}
	newVersion, err := d.(*dockerUpdater).fetchLatest(pkgParsed)
	if err != nil {
		t.Fatal("Valid semver should not throw error", err)
	}
	if newVersion != "1.0.2" {
		t.Fatal("fetch tag must return latest tag")
	}
	if pkgParsed.oldVersion != "latest" {
		t.Fatal("no version means latest in docker wolrd")
	}
}

func TestFetchLatestWithoutSemver(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		return []string{"a", "b", "c"}, nil
	}
	pkgParsed, err := d.(*dockerUpdater).parseDockerPkg("")
	if err != nil {
		t.Fatal("Valid pkg should not throw error", err)
	}
	newVersion, err := d.(*dockerUpdater).fetchLatest(pkgParsed)
	if err != nil {
		t.Fatal("Valid repository should not throw error", err)
	}
	if newVersion != "" {
		t.Fatal("no semver should return empty string")
	}
	if pkgParsed.oldVersion != "latest" {
		t.Fatal("no version means latest in docker wolrd")
	}
}

func buildTest(filepath string, from string, d Updater, excludes []string) ([]project.Dependency, []byte, error) {
	ioutil.WriteFile(filepath, []byte(from), 0777)
	dependencies, _ := d.Update("", project.Package{Path: filepath, Type: project.PackageType("")}, excludes)
	data, err := ioutil.ReadFile(filepath)
	return dependencies, data, err
}

func TestUpdate(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		return []string{"1.0.2", "0.5", "1.0.0", "pre-2.0.0"}, nil
	}
	dependencies, res, err := buildTest("/tmp/test1", `FROM scratch
	
	RUN aptget`, d, []string{})
	if err != nil {
		t.Fatal("File should be read", err)
	}
	test := `FROM scratch:1.0.2
	
	RUN aptget`
	if !bytes.Equal(bytes.Trim(res, "\n"), []byte(test)) {
		t.Fatal("Update should update file ", string(res), " instead of ", test)
	}

	if len(dependencies) != 1 {
		t.Fatal("Should return 1 dep")
	}
	if dependencies[0].Name != "scratch" {
		t.Fatalf("Should be scratch instead of %s", dependencies[0].Name)
	}
	if dependencies[0].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[0].OriginalVersion)
	}
	if dependencies[0].NewVersion != "1.0.2" {
		t.Fatalf("Should be 1.0.2 instead of %s", dependencies[0].NewVersion)
	}
}

func TestUpdateMultiFrom(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		if repository == "library/golang" {
			return []string{"36.65.69"}, nil
		}
		return []string{"28.27.26"}, nil
	}
	dependencies, res, err := buildTest("/tmp/test1", `FROM golang:1.7.3 as builder
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html  
COPY app.go    .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/github.com/alexellis/href-counter/app .
CMD ["./app"]`, d, []string{})
	if err != nil {
		t.Fatal("File should be read", err)
	}
	test := `FROM golang:36.65.69 as builder
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html  
COPY app.go    .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:28.27.26  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/github.com/alexellis/href-counter/app .
CMD ["./app"]`
	if !bytes.Equal(bytes.Trim(res, "\n"), []byte(test)) {
		dmp := diffmatchpatch.New()
		diffs := dmp.DiffMain(string(res), test, true)
		fmt.Println(dmp.DiffPrettyText(diffs))
		t.Fatal("Update should update file ", string(res), " instead of ", test)
	}
	if len(dependencies) != 2 {
		t.Fatal("should return 2 dependencies updated")
	}
	if dependencies[0].Name != "golang" {
		t.Fatalf("Should be golang instead of %s", dependencies[0].Name)
	}
	if dependencies[0].OriginalVersion != "1.7.3" {
		t.Fatalf("Should be 1.7.3 instead of %s", dependencies[0].OriginalVersion)
	}
	if dependencies[0].NewVersion != "36.65.69" {
		t.Fatalf("Should be 36.65.69 instead of %s", dependencies[0].NewVersion)
	}
	if dependencies[1].Name != "alpine" {
		t.Fatalf("Should be alpine instead of %s", dependencies[1].Name)
	}
	if dependencies[1].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[1].OriginalVersion)
	}
	if dependencies[1].NewVersion != "28.27.26" {
		t.Fatalf("Should be 28.27.26 instead of %s", dependencies[1].NewVersion)
	}
}

func TestUpdateWithVersion(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		return []string{"1.0.2", "0.5", "1.0.0", "pre-2.0.0"}, nil
	}
	dependencies, res, err := buildTest("/tmp/test1.1", "FROM scratch:latest", d, []string{})
	if err != nil {
		t.Fatal("File should be read", err)
	}
	test := "FROM scratch:1.0.2"
	if !bytes.Equal(bytes.Trim(res, "\n"), []byte(test)) {
		t.Fatal("Update should update file ", string(res), " instead of ", test)
	}

	if len(dependencies) != 1 {
		t.Fatal("Should return 1 dep")
	}
	if dependencies[0].Name != "scratch" {
		t.Fatalf("Should be scratch instead of %s", dependencies[0].Name)
	}
	if dependencies[0].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[0].OriginalVersion)
	}
	if dependencies[0].NewVersion != "1.0.2" {
		t.Fatalf("Should be 1.0.2 instead of %s", dependencies[0].NewVersion)
	}
}

func TestUpdateWithoutSemver(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		return []string{"a", "b", "c"}, nil
	}
	dependencies, res, err := buildTest("/tmp/test2", "FROM scratch", d, []string{})
	if err != nil {
		t.Fatal("File should be read", err)
	}
	test := "FROM scratch"
	if !bytes.Equal(bytes.Trim(res, "\n"), []byte(test)) {
		t.Fatal("Update should update file ", string(res), " instead of ", test)
	}

	if len(dependencies) != 0 {
		t.Fatal("Should return 0 dep")
	}
}

func TestUpdateWithPersonalRegistry(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		if registryURL != "registryURL:port" {
			t.Fatal("Docker should call valid registry")
		}
		return []string{"1.0.2", "2.0.0", "2.5.6"}, nil
	}
	dependencies, res, err := buildTest("/tmp/test2", "FROM registryURL:port/scope/scratch", d, []string{})
	if err != nil {
		t.Fatal("File should be read", err)
	}
	test := "FROM registryURL:port/scope/scratch:2.5.6"
	if !bytes.Equal(bytes.Trim(res, "\n"), []byte(test)) {
		t.Fatal("Update should update file ", string(res), " instead of ", test)
	}

	if len(dependencies) != 1 {
		t.Fatal("Should return 1 dep")
	}
	if dependencies[0].Name != "scope/scratch" {
		t.Fatalf("Should be scope/scratch instead of %s", dependencies[0].Name)
	}
	if dependencies[0].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[0].OriginalVersion)
	}
	if dependencies[0].NewVersion != "2.5.6" {
		t.Fatalf("Should be 2.5.6 instead of %s", dependencies[0].NewVersion)
	}
}

func TestUpdateWithPersonalRegistryWithoutSemver(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		if registryURL != "registryURL:port" {
			t.Fatal("Docker should call valid registry")
		}
		return []string{"a", "b", "c"}, nil
	}
	dependencies, res, err := buildTest("/tmp/test2", "FROM registryURL:port/scope/scratch", d, []string{})
	if err != nil {
		t.Fatal("File should be read", err)
	}
	test := "FROM registryURL:port/scope/scratch"
	if !bytes.Equal(bytes.Trim(res, "\n"), []byte(test)) {
		t.Fatal("Update should update file ", string(res), " instead of ", test)
	}

	if len(dependencies) != 0 {
		t.Fatal("Should return 0 dep")
	}
}

func TestUpdateExcludes(t *testing.T) {
	d := newDocker(DefaultConfig)
	d.(*dockerUpdater).fetchTags = func(registryURL string, repository string) (tags []string, err error) {
		if registryURL != "registryURL:port" {
			t.Fatal("Docker should call valid registry")
		}
		return []string{"1.0.6"}, nil
	}
	dependencies, res, err := buildTest("/tmp/test2", "FROM registryURL:port/scope/scratch", d, []string{"scope/scratch"})
	if err != nil {
		t.Fatal("File should be read", err)
	}
	test := "FROM registryURL:port/scope/scratch"
	if !bytes.Equal(bytes.Trim(res, "\n"), []byte(test)) {
		t.Fatal("Update should update file ", string(res), " instead of ", test)
	}

	if len(dependencies) != 0 {
		t.Fatal("Should return 0 dep")
	}
}
