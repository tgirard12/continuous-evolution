package updaters

import (
	"bytes"
	"continuous-evolution/src/project"
	"io/ioutil"
	"os"
	"regexp"
	"strings"

	"errors"

	"github.com/blang/semver"
	"github.com/heroku/docker-registry-client/registry"
	"github.com/sirupsen/logrus"
)

//DockerConfig is configuration required by docker updater
type DockerConfig struct {
	Enabled  bool
	Registry string
	Scope    string
}

//DefaultDockerConfig is the default configuration required by docker updater
var DefaultDockerConfig = DockerConfig{Enabled: true, Registry: "https://registry-1.docker.io", Scope: "library"}

var loggerDocker = logrus.WithField("logger", "updaters/docker")

var regexpFrom = regexp.MustCompile(`(?m:^FROM\s+([^\s]+)(.*)$)`)

func dockerIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "Dockerfile"
}

type dockerUpdater struct {
	config    DockerConfig
	hubs      map[string]*registry.Registry
	fetchTags func(registryURL string, repository string) (tags []string, err error)
}

func newDocker(config Config) Updater {
	u := &dockerUpdater{
		config: config.Docker,
		hubs:   make(map[string]*registry.Registry),
	}
	u.fetchTags = u.lazyHub
	return u
}

func (u *dockerUpdater) lazyHub(registryURL string, repository string) (tags []string, err error) {
	if u.hubs[registryURL] == nil {
		var hub *registry.Registry
		var err error
		if registryURL == u.config.Registry {
			hub, err = registry.New(registryURL, "", "")
		} else {
			hub, err = registry.NewInsecure("http://"+registryURL, "", "")
		}
		if err != nil {
			loggerDocker.WithError(err).WithField("registryURL", registryURL).Error("Can't build registry")
			return nil, err
		}
		u.hubs[registryURL] = hub
	}
	return u.hubs[registryURL].Tags(repository)
}

func (u *dockerUpdater) GetType() project.PackageType {
	return project.PackageType("docker")
}

func (u *dockerUpdater) Update(_ string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	newDepth := make([]project.Dependency, 0)
	data, err := ioutil.ReadFile(pkg.Path)
	if err != nil {
		loggerDocker.WithField("file", pkg.Path).WithError(err).Error("Can't read file")
	}
	results := regexpFrom.FindAllSubmatch(data, -1)
	for _, result := range results {
		name := string(result[1])
		endLine := result[2]
		pkgParsed, err := u.parseDockerPkg(name)
		if err != nil {
			loggerDocker.WithError(err).Error("Can't parse version")
		}
		if !u.isExcludes(pkgParsed.displayName(u.config), excludes) {
			newVersion, err := u.fetchLatest(pkgParsed)
			if err != nil {
				loggerDocker.WithError(err).Error("Can't fetch version")
			}
			if newVersion != "" {
				newDepth = append(newDepth, project.Dependency{Name: pkgParsed.displayName(u.config), NewVersion: newVersion, OriginalVersion: pkgParsed.oldVersion, CanBeExcludes: true})
				data = bytes.Replace(data, result[0], pkgParsed.toFile(newVersion, u.config, []byte("FROM "), endLine), 1)
				err := ioutil.WriteFile(pkg.Path, data, 0666)
				if err != nil {
					loggerDocker.WithError(err).Error("Can't fetch version")
				}
			}
		}
	}
	if len(results) == 0 {
		loggerDocker.WithField("Dockerfile", string(data)).Warn("Dockerfile without from !?")
	}
	return newDepth, nil
}

type dockerPkg struct {
	registryURL string
	scope       string
	project     string
	oldVersion  string
}

func (pkg dockerPkg) displayName(config DockerConfig) string {
	if pkg.scope == config.Scope {
		return pkg.project
	}
	return pkg.scope + "/" + pkg.project
}

func (pkg dockerPkg) toFile(newVersion string, config DockerConfig, prepend []byte, endLine []byte) []byte {
	res := make([]byte, len(prepend))
	copy(res, prepend)
	if pkg.registryURL != config.Registry {
		res = append(res, []byte(pkg.registryURL+"/")...)
	}
	if pkg.scope != config.Scope {
		res = append(res, []byte(pkg.scope+"/")...)
	}
	res = append(res, []byte(pkg.project)...)
	if newVersion != "" {
		res = append(res, []byte(":"+newVersion)...)
	}
	return append(res, endLine...)
}

//parseDockerPkg take full docker pkg (e.g. registryURL?/scope?/project)
func (u *dockerUpdater) parseDockerPkg(dockerPkgName string) (dockerPkg, error) {
	parts := strings.Split(dockerPkgName, "/")
	registryURL := u.config.Registry
	scope := u.config.Scope
	project := ""
	oldVersion := "latest"
	if len(parts) == 1 { // FROM project
		project = parts[0]
	} else if len(parts) == 2 { // FROM scope/project || FROM url/project
		if strings.Contains(parts[0], ".") || strings.Index(parts[0], "localhost") == 0 {
			registryURL = parts[0]
		} else {
			scope = parts[0]
		}
		project = parts[1]
	} else if len(parts) == 3 { // FROM url/scope/project
		registryURL = parts[0]
		scope = parts[1]
		project = parts[2]
	} else {
		return dockerPkg{}, errors.New("valid docker image must not have more than 3 parts url/scope/project")
	}
	nameAndVersion := strings.Split(project, ":")
	if len(nameAndVersion) > 1 {
		project = nameAndVersion[0]
		oldVersion = nameAndVersion[1]
	}
	return dockerPkg{
		registryURL: registryURL,
		scope:       scope,
		project:     project,
		oldVersion:  oldVersion,
	}, nil
}

//fetchLatest
func (u *dockerUpdater) fetchLatest(pkg dockerPkg) (string, error) {
	latestOriginalVersion := ""
	if strings.Index(pkg.registryURL, "127.0.0.1") != 0 && strings.Index(pkg.registryURL, "localhost") != 0 {
		loggerDocker.WithField("dockerPkg", pkg.project).Info("fetch docker versions")
		tags, err := u.fetchTags(pkg.registryURL, pkg.scope+"/"+pkg.project)
		if err != nil {
			return "", err
		}
		loggerDocker.WithField("tags", tags).Info("tags from hub")

		var latestVersion semver.Version
		for _, version := range tags {
			parsedVersion, err := semver.ParseTolerant(version)
			if err != nil {
				loggerDocker.WithError(err).Debug("not semver version")
				continue
			}
			if latestOriginalVersion == "" || len(parsedVersion.Pre) == 0 && parsedVersion.GT(latestVersion) {
				latestVersion = parsedVersion
				latestOriginalVersion = version
			}
		}
	}

	loggerDocker.WithField("tag", latestOriginalVersion).Info("tag selected")
	return latestOriginalVersion, nil
}

func (u *dockerUpdater) isExcludes(name string, excludes []string) bool {
	for _, exclude := range excludes {
		if exclude == name {
			return true
		}
	}
	return false
}
