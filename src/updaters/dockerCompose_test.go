package updaters

import (
	"errors"
	"testing"
)

const data = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  conevol:
    image:conevol
`

const goodData = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq:1.0
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  conevol:
    image: conevol:0.2

`

func TestUpdateDockerCompose(t *testing.T) {
	d := newDockerCompose(DefaultConfig)
	d.(*dockerComposeUpdater).fetchLatestDockerTag = func(pkg dockerPkg) (string, error) {
		if pkg.scope == "nsqio" && pkg.project == "nsq" {
			return "1.0", nil
		} else if pkg.scope == "library" && pkg.project == "conevol" {
			return "0.2", nil
		}
		return "", errors.New("You call method with bad argument")
	}
	_, newData := d.(*dockerComposeUpdater).updateServices([]byte(data), []string{})
	if newData != goodData {
		t.Fatal("Bad updated data", newData)
	}
}

const dataMultipleSameService = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  nsqd:
    image: nsqio/nsq
    command: /nsqd --lookupd-tcp-address=nsqlookupd:4160
    ports:
      - "4150:4150"
      - "4151"
    depends_on: 
      - nsqlookupd

  conevol:
    image: conevol:0.2
`

const goodDataMultipleSameService = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq:1.0
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  nsqd:
    image: nsqio/nsq:1.0
    command: /nsqd --lookupd-tcp-address=nsqlookupd:4160
    ports:
      - "4150:4150"
      - "4151"
    depends_on: 
      - nsqlookupd

  conevol:
    image: conevol:0.8

`

func TestUpdateDockerComposeMultiple(t *testing.T) {
	d := newDockerCompose(DefaultConfig)
	d.(*dockerComposeUpdater).fetchLatestDockerTag = func(pkg dockerPkg) (string, error) {
		if pkg.scope == "nsqio" && pkg.project == "nsq" {
			return "1.0", nil
		} else if pkg.scope == "library" && pkg.project == "conevol" && pkg.oldVersion == "0.2" {
			return "0.8", nil
		}
		return "", errors.New("You call method with bad argument")
	}
	dependencies, newData := d.(*dockerComposeUpdater).updateServices([]byte(dataMultipleSameService), []string{})
	if newData != goodDataMultipleSameService {
		t.Fatal("Bad updated data", newData)
	}
	if len(dependencies) != 3 {
		t.Fatal("Should return 3 dep")
	}
	if dependencies[0].Name != "nsqio/nsq" {
		t.Fatalf("Should be nsqio/nsq instead of %s", dependencies[0].Name)
	}
	if dependencies[0].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[0].OriginalVersion)
	}
	if dependencies[0].NewVersion != "1.0" {
		t.Fatalf("Should be 1.0 instead of %s", dependencies[0].NewVersion)
	}
	if dependencies[1].Name != "nsqio/nsq" {
		t.Fatalf("Should be nsqio/nsq instead of %s", dependencies[1].Name)
	}
	if dependencies[1].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[1].OriginalVersion)
	}
	if dependencies[1].NewVersion != "1.0" {
		t.Fatalf("Should be 1.0 instead of %s", dependencies[1].NewVersion)
	}
	if dependencies[2].Name != "conevol" {
		t.Fatalf("Should be conevol instead of %s", dependencies[2].Name)
	}
	if dependencies[2].OriginalVersion != "0.2" {
		t.Fatalf("Should be 0.2 instead of %s", dependencies[2].OriginalVersion)
	}
	if dependencies[2].NewVersion != "0.8" {
		t.Fatalf("Should be 0.8 instead of %s", dependencies[2].NewVersion)
	}
}

const goodDataMultipleServiceExcudes = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq:1.0
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  nsqd:
    image: nsqio/nsq:1.0
    command: /nsqd --lookupd-tcp-address=nsqlookupd:4160
    ports:
      - "4150:4150"
      - "4151"
    depends_on: 
      - nsqlookupd

  conevol:
    image: conevol:0.2

`

func TestUpdateDockerComposeMultipleWithExcludes(t *testing.T) {
	d := newDockerCompose(DefaultConfig)
	d.(*dockerComposeUpdater).fetchLatestDockerTag = func(pkg dockerPkg) (string, error) {
		if pkg.scope == "nsqio" && pkg.project == "nsq" {
			return "1.0", nil
		} else if pkg.scope == "library" && pkg.project == "conevol" && pkg.oldVersion == "0.2" {
			return "0.8", nil
		}
		return "", errors.New("You call method with bad argument")
	}
	dependencies, newData := d.(*dockerComposeUpdater).updateServices([]byte(dataMultipleSameService), []string{"conevol"})
	if newData != goodDataMultipleServiceExcudes {
		t.Fatal("Bad updated data", newData)
	}
	if len(dependencies) != 2 {
		t.Fatal("Should return 2 dep")
	}
	if dependencies[0].Name != "nsqio/nsq" {
		t.Fatalf("Should be nsqio/nsq instead of %s", dependencies[0].Name)
	}
	if dependencies[0].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[0].OriginalVersion)
	}
	if dependencies[0].NewVersion != "1.0" {
		t.Fatalf("Should be 1.0 instead of %s", dependencies[0].NewVersion)
	}
	if dependencies[1].Name != "nsqio/nsq" {
		t.Fatalf("Should be nsqio/nsq instead of %s", dependencies[1].Name)
	}
	if dependencies[1].OriginalVersion != "latest" {
		t.Fatalf("Should be latest instead of %s", dependencies[1].OriginalVersion)
	}
	if dependencies[1].NewVersion != "1.0" {
		t.Fatalf("Should be 1.0 instead of %s", dependencies[1].NewVersion)
	}
}
