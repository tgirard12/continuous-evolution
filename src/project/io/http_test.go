package io

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"testing"
)

type ClosingBuffer struct {
	*bytes.Buffer
}

func (cb *ClosingBuffer) Close() (err error) {
	return
}

func TestGet(t *testing.T) {
	h := NewHTTP("login", "token")
	h.requestFabric = func(method, url string, body io.Reader) (*http.Request, error) {
		if method != "GET" {
			t.Fatal("http.Get should make a get request")
		}
		if url != "http://my.url" {
			t.Fatal("http.Get should call with good url")
		}
		if body != nil {
			t.Fatal("http.Get should send nil body")
		}
		return http.NewRequest(method, url, body)
	}
	h.do = func(req *http.Request) (*http.Response, error) {
		return &http.Response{Body: &ClosingBuffer{bytes.NewBufferString("{}")}}, nil
	}
	res, err := h.Get("http://my.url")
	if err != nil {
		t.Fatal("Good http call should not throw error", err)
	}
	if string(res) != "{}" {
		t.Fatal("Get should return body result {} instead of ", string(res))
	}
}

func TestPostPatchPut(t *testing.T) {
	h := NewHTTP("login", "token")
	tests := []struct {
		exec      func(string, []byte) ([]byte, error)
		method    string
		errFabric error
		errDo     error
	}{
		{exec: h.Post, method: "POST", errFabric: nil, errDo: nil},
		{exec: h.Post, method: "POST", errFabric: errors.New("fake fabric error"), errDo: nil},
		{exec: h.Post, method: "POST", errFabric: nil, errDo: errors.New("fake do error")},
		{exec: h.Patch, method: "PATCH", errFabric: nil, errDo: nil},
		{exec: h.Patch, method: "PATCH", errFabric: errors.New("fake fabric error"), errDo: nil},
		{exec: h.Patch, method: "PATCH", errFabric: nil, errDo: errors.New("fake do error")},
		{exec: h.Put, method: "PUT", errFabric: nil, errDo: nil},
		{exec: h.Put, method: "PUT", errFabric: errors.New("fake fabric error"), errDo: nil},
		{exec: h.Put, method: "PUT", errFabric: nil, errDo: errors.New("fake do error")},
	}
	for _, test := range tests {
		h.requestFabric = func(method, url string, body io.Reader) (*http.Request, error) {
			if method != test.method {
				t.Fatalf("%s should make a %s request", test.method, method)
			}
			if url != "http://my.url" {
				t.Fatalf("%s should call with good url", test.method)
			}
			if body == nil {
				t.Fatalf("%s should send nil body", test.method)
			}
			if test.errFabric != nil {
				return nil, test.errFabric
			}
			return http.NewRequest(method, url, body)
		}
		h.do = func(req *http.Request) (*http.Response, error) {
			return &http.Response{Body: &ClosingBuffer{bytes.NewBufferString("{}")}}, test.errDo
		}
		res, err := test.exec("http://my.url", []byte(`{"send":"ok"}`))
		if test.errFabric != nil {
			if err == nil {
				t.Fatalf("[%s] Error in fabric should throw error", test.method)
			}
			if err.Error() != test.errFabric.Error() {
				t.Fatal("Error in fabric should throw good error", err)
			}
		}
		if test.errDo != nil {
			if err == nil {
				t.Fatal("Error in do should throw error")
			}
			if err.Error() != test.errDo.Error() {
				t.Fatal("Error in do should throw good error", err)
			}
		}
		if test.errFabric == nil && test.errDo == nil {
			if err != nil {
				t.Fatal("Good http call should not throw error", err)
			}
			if string(res) != "{}" {
				t.Fatalf("%s should return body result {} instead of %s", test.method, string(res))
			}
		}
	}
}

func TestFabricError(t *testing.T) {
	h := NewHTTP("login", "token")
	h.requestFabric = func(method, url string, body io.Reader) (*http.Request, error) {
		return nil, errors.New("fake error")
	}
	h.do = func(req *http.Request) (*http.Response, error) {
		return &http.Response{Body: &ClosingBuffer{bytes.NewBufferString("{}")}}, nil
	}
	_, err := h.Get("http://my.url")
	if err == nil {
		t.Fatal("Error in fabric should throw error")
	}
	if err.Error() != "fake error" {
		t.Fatal("Error in fabric should throw good error", err)
	}
}

func TestDoError(t *testing.T) {
	h := NewHTTP("login", "token")
	h.requestFabric = func(method, url string, body io.Reader) (*http.Request, error) {
		return http.NewRequest(method, url, body)
	}
	h.do = func(req *http.Request) (*http.Response, error) {
		return nil, errors.New("fake do error")
	}
	_, err := h.Get("http://my.url")
	if err == nil {
		t.Fatal("Error in do should throw error")
	}
	if err.Error() != "fake do error" {
		t.Fatal("Error in do should throw good error", err)
	}
}
