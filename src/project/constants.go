package project

//Constants : if you don't want in your code just copy here :D
const (
	WebURLAddProject   = "addProject"
	WebURLMergeRequest = "mergeRequest"
	MergeRequestTitle  = "Update dependencies"
	MergeRequestBody   = `New dependencies versions :{{- "\n" -}}
{{- $project := . -}}
{{- range $i, $pkg := .Packages -}}
{{- if $pkg.IsExcludes $project -}}
{{- "\n" -}}* [ ] {{$pkg.CleanPath $project}}
{{- else -}}
{{- "\n" -}}* [x] {{$pkg.CleanPath $project}}{{if $pkg.UpdatedDependencies}} :{{- "\n" -}}
{{- range $i2, $depth := $pkg.UpdatedDependencies -}}
{{- if $depth.IsExcludes $pkg $project -}}
{{- "    " -}}* [ ] {{$depth.Name}} : {{if $depth.OriginalVersion}}{{$depth.OriginalVersion}} => {{end}}` + "`" + `{{$depth.NewVersion}}` + "`" + `{{- "\n" -}}
{{- else if $depth.CanBeExcludes -}}
{{- "    " -}}* [x] {{$depth.Name}} : {{if $depth.OriginalVersion}}{{$depth.OriginalVersion}} => {{end}}` + "`" + `{{$depth.NewVersion}}` + "`" + `{{- "\n" -}}
{{- else -}}
{{- "    " -}}* {{$depth.Name}} : {{if $depth.OriginalVersion}}{{$depth.OriginalVersion}} => {{end}}` + "`" + `{{$depth.NewVersion}}` + "`" + `{{- "\n" -}}
{{- end -}}
{{- end -}}
{{- end -}}
{{- end -}}
{{- end -}}{{- "\n\n" -}}
{{- if ne .URLReProcess "" -}}[Relaunch update]({{.URLReProcess}}){{- "\n\n" -}}{{- end -}}
> This merge request is generate, no human has tested the resulting code. If you are confident with your CI accept it, else fetch the result and test it manually. By the way adding CI is always a good idea ;){{- "\n\n" -}}
Gracefully updated by [ContinuousEvolution](https://gitlab.com/ContinuousEvolution/continuous-evolution) with :heart:{{- "\n\n" -}}
> If you close this merge-request, ContinuousEvolution will remove this project from it database. No more update will be delivered until you resubscribe.`
)
