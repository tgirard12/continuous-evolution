package reporters

import (
	"bytes"
	"continuous-evolution/src/project"
	"encoding/json"
	"fmt"
	"html/template"
	"net/url"
	"strconv"

	"github.com/sirupsen/logrus"
)

//GitlabConfig is configuration required by gitlab reporter
type GitlabConfig struct {
	Enabled bool
}

//DefaultGitlabConfig is the default configuration required by gitlab reporter
var DefaultGitlabConfig = GitlabConfig{Enabled: true}

var loggerGitlab = logrus.WithField("logger", "reporters/gitlab")

type gitlab struct {
	git            Reporter
	advertizedHost string
}

type gitlabMergeRequest struct {
	ID           string `json:"id"`
	Title        string `json:"title"`
	SourceBranch string `json:"source_branch"`
	TargetBranch string `json:"target_branch"`
	Description  string `json:"description"`
}

func newGitlab(config Config) Reporter {
	return gitlab{
		git:            newGit(),
		advertizedHost: config.Advertizedhost,
	}
}

func (g gitlab) Accept(project project.Project) bool {
	//TODO call api to know if its really a gitlab server
	return project.TypeHost != "github.com" && project.Login != "" && project.Token != "" && g.git.Accept(project)
}

func (g gitlab) Report(p project.Project, tplMergeRequestBody *template.Template) (project.Project, error) {
	defaultBranch, err := p.Git().DefaultBranch()
	if err != nil {
		return p, err
	}

	//gitlab need git reporter
	if _, err := g.git.Report(p, tplMergeRequestBody); err != nil {
		return p, err
	}

	id := url.PathEscape(p.Organisation + "/" + p.Name)

	if p.ReProcessDistantID == "" {
		var mergeRequestBody bytes.Buffer
		err = tplMergeRequestBody.Execute(&mergeRequestBody, p)
		if err != nil {
			return p, err
		}

		urlStr := fmt.Sprintf("https://%s/api/v4/projects/%s/merge_requests?private_token=%s", p.TypeHost, id, p.Token)
		//jsonStr = []byte(fmt.Sprintf(`{"id":"%s","title":"%s","source_branch":"%s","target_branch":"%s","description":"%s"}`, id, project.MergeRequestTitle, project.BranchName, defaultBranch, mergeRequestBody.Bytes()))
		gmr := gitlabMergeRequest{
			ID:           id,
			Title:        project.MergeRequestTitle,
			SourceBranch: p.BranchName,
			TargetBranch: defaultBranch,
			Description:  mergeRequestBody.String(),
		}
		jsonStr, err := json.Marshal(gmr)
		if err != nil {
			return p, err
		}
		loggerGitlab.WithField("url", urlStr).Info("send http post")
		jsonRes, err := p.HTTP().Post(urlStr, jsonStr)
		if err != nil {
			loggerGitlab.WithField("url", urlStr).WithField("body", string(jsonRes)).WithError(err).Info("http post body")
			return p, err
		}
		//edit merge-request to add direct link
		objectRes := make(map[string]interface{})
		err = json.Unmarshal(jsonRes, &objectRes)
		if err != nil {
			return p, err
		}
		iidMergeRequest := objectRes["iid"].(float64)
		p = p.SetReProcessDistantID(strconv.FormatFloat(iidMergeRequest, 'f', 0, 64))
	}

	if p.ReProcessDistantID != "" {
		urlStr := fmt.Sprintf("https://%s/api/v4/projects/%s/merge_requests/%s", p.TypeHost, id, p.ReProcessDistantID)
		url := fmt.Sprintf("%s/%s?project=%s", g.advertizedHost, project.WebURLMergeRequest, urlStr)
		p = p.SetURLReProcess(url)
		var mergeRequestBodyWithUpdate bytes.Buffer
		err := tplMergeRequestBody.Execute(&mergeRequestBodyWithUpdate, p)
		if err != nil {
			return p, err
		}
		gmr := gitlabMergeRequest{
			ID:           id,
			Title:        project.MergeRequestTitle,
			SourceBranch: p.BranchName,
			TargetBranch: defaultBranch,
			Description:  mergeRequestBodyWithUpdate.String(),
		}
		jsonStr, err := json.Marshal(gmr)
		if err != nil {
			return p, err
		}
		loggerGitlab.WithField("url", urlStr).Info("send http patch")
		jsonRes, err := p.HTTP().Put(fmt.Sprintf("%s?private_token=%s", urlStr, p.Token), jsonStr)
		if err != nil {
			loggerGitlab.WithField("url", urlStr).WithField("body", string(jsonRes)).WithError(err).Error("http patch body")
			return p, err
		}
		urlForScehdulerStr := fmt.Sprintf("https://%s:%s@%s/api/v4/projects/%s/%s/merge_requests/%s", p.Login, p.Token, p.TypeHost, p.Organisation, p.Name, p.ReProcessDistantID)
		p = p.SetGitURL(urlForScehdulerStr)
	}

	return p, nil
}
