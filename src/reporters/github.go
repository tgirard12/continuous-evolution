package reporters

import (
	"bytes"
	"continuous-evolution/src/project"
	"encoding/json"
	"fmt"
	"html/template"
	"strconv"

	"github.com/sirupsen/logrus"
)

//GithubConfig is configuration required by github reporter
type GithubConfig struct {
	Enabled bool
}

//DefaultGithubConfig is the default configuration required by github reporter
var DefaultGithubConfig = GithubConfig{Enabled: true}

var loggerGithub = logrus.WithField("logger", "reporters/github")

type github struct {
	git            Reporter
	advertizedHost string
}

type githubPullRequest struct {
	Title               string `json:"title"`
	Head                string `json:"head"`
	Base                string `json:"base"`
	Body                string `json:"body"`
	MaintainerCanModify bool   `json:"maintainer_can_modify"`
}

func newGithub(config Config) Reporter {
	return github{
		git:            newGit(),
		advertizedHost: config.Advertizedhost,
	}
}

func (g github) Accept(project project.Project) bool {
	return project.TypeHost == "github.com" && project.Login != "" && project.Token != "" && g.git.Accept(project)
}

func (g github) Report(p project.Project, tplMergeRequestBody *template.Template) (project.Project, error) {
	defaultBranch, err := p.Git().DefaultBranch()
	if err != nil {
		return p, err
	}

	//github need git reporter
	if _, err := g.git.Report(p, tplMergeRequestBody); err != nil {
		return p, err
	}

	if p.ReProcessDistantID == "" {
		var mergeRequestBody bytes.Buffer
		err = tplMergeRequestBody.Execute(&mergeRequestBody, p)
		if err != nil {
			return p, err
		}

		urlStr := fmt.Sprintf("https://api.github.com/repos/%s/%s/pulls", p.Organisation, p.Name)
		gmr := githubPullRequest{
			Title:               project.MergeRequestTitle,
			Head:                p.BranchName,
			Base:                defaultBranch,
			Body:                mergeRequestBody.String(),
			MaintainerCanModify: true,
		}
		jsonStr, err := json.Marshal(gmr)
		if err != nil {
			return p, err
		}
		loggerGithub.WithField("url", urlStr).Info("send http post")
		jsonRes, err := p.HTTP().Post(urlStr, jsonStr)
		if err != nil {
			loggerGithub.WithField("url", urlStr).WithField("body", string(jsonRes)).WithError(err).Info("http post body")
			return p, err
		}
		//edit pull-request to add direct link
		objectRes := make(map[string]interface{})
		err = json.Unmarshal(jsonRes, &objectRes)
		if err != nil {
			return p, err
		}
		id := objectRes["number"].(float64)
		p = p.SetReProcessDistantID(strconv.FormatFloat(id, 'f', 0, 64))
	}

	if p.ReProcessDistantID != "" {
		urlStr := fmt.Sprintf("https://api.github.com/repos/%s/%s/pulls/%s", p.Organisation, p.Name, p.ReProcessDistantID)
		url := fmt.Sprintf("%s/%s?project=%s", g.advertizedHost, project.WebURLMergeRequest, urlStr)
		p = p.SetURLReProcess(url)
		var mergeRequestBodyWithUpdate bytes.Buffer
		err = tplMergeRequestBody.Execute(&mergeRequestBodyWithUpdate, p)
		if err != nil {
			return p, err
		}
		loggerGithub.WithField("project", p).WithField("body", mergeRequestBodyWithUpdate.String()).Warn("debug")
		gmr := githubPullRequest{
			Title:               project.MergeRequestTitle,
			Head:                p.BranchName,
			Base:                defaultBranch,
			Body:                mergeRequestBodyWithUpdate.String(),
			MaintainerCanModify: true,
		}
		jsonStr, err := json.Marshal(gmr)
		if err != nil {
			return p, err
		}
		jsonRes, err := p.HTTP().Patch(urlStr, jsonStr)
		if err != nil {
			loggerGithub.WithField("url", urlStr).WithField("bodySend", string(jsonStr)).WithField("bodyRet", string(jsonRes)).WithError(err).Error("send http patch")
			return p, err
		}
		urlForScehdulerStr := fmt.Sprintf("https://%s:%s@api.github.com/repos/%s/%s/pulls/%s", p.Login, p.Token, p.Organisation, p.Name, p.ReProcessDistantID)
		p = p.SetGitURL(urlForScehdulerStr)
	}

	return p, nil
}
