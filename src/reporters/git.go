package reporters

import (
	"continuous-evolution/src/project"
	"html/template"
)

//GitConfig is configuration required by git reporter
type GitConfig struct {
	Enabled bool
}

//DefaultGitConfig is the default configuration required by git reporter
var DefaultGitConfig = GitConfig{Enabled: true}

type git struct{}

func newGit() Reporter {
	return git{}
}

func (g git) Accept(project project.Project) bool {
	return project.Git().Diff()
}

func (g git) Report(project project.Project, _ *template.Template) (project.Project, error) {
	if !project.BranchAlreadyExist {
		if err := project.Git().NewBranch(); err != nil {
			return project, err
		}
	}
	if err := project.Git().Commit(); err != nil {
		return project, err
	}
	if err := project.Git().PushForce(); err != nil {
		return project, err
	}
	return project, nil
}
