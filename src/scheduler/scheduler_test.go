package scheduler

import (
	"continuous-evolution/src/project"
	"encoding/json"
	"io/ioutil"
	"testing"
	"time"
)

func TestBadConfig(t *testing.T) {
	config := Config{Enabled: true, WaitDurationProcess: "1s"}
	_, err := NewScheduler(config)
	if err == nil {
		t.Fatal("Bad config (no bdpath) must return an error")
	}
	config = Config{Enabled: true, PathDb: DefaultConfig.PathDb, WaitDurationProcess: "a"}
	_, err = NewScheduler(config)
	if err == nil {
		t.Fatal("Bad config (ba wait time) must return an error")
	}
}

func TestNextProject(t *testing.T) {
	config := Config{Enabled: true, PathDb: DefaultConfig.PathDb, WaitDurationProcess: "100s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid")
	}
	s.(*scheduler).projects = []minimalProject{{LastUpdatedDate: time.Now().Add(-10 * time.Second), Name: "test", GitURL: "test"}}
	ok, p, duration := s.(*scheduler).nextProjectToUpdate()
	if !ok {
		t.Fatal("Scheduler with project must found a next project")
	}
	if p.Name != "test" {
		t.Fatal("Scheduler must select valid project test instead of", p.Name)
	}
	if int(duration.Seconds()) < 89 && int(duration.Seconds()) < 91 {
		t.Fatal("Scheduler must sleep during 90sc before update project instead of", duration.Seconds())
	}
}

func TestNextProjectWith2Projects(t *testing.T) {
	config := Config{Enabled: true, PathDb: DefaultConfig.PathDb, WaitDurationProcess: "100s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid")
	}
	s.(*scheduler).projects = []minimalProject{{LastUpdatedDate: time.Now().Add(-10 * time.Second), Name: "test", GitURL: "test"}, {LastUpdatedDate: time.Now().Add(-5 * time.Second), Name: "test2", GitURL: "test2"}}
	ok, p, duration := s.(*scheduler).nextProjectToUpdate()
	if !ok {
		t.Fatal("Scheduler with project must found a next project")
	}
	if p.Name != "test" {
		t.Fatal("Scheduler must select valid project test instead of", p.Name)
	}
	if int(duration.Seconds()) > 89 && int(duration.Seconds()) < 91 {
		t.Fatal("Scheduler must sleep during 90sc before update project instead of", duration.Seconds())
	}
	s.Save(project.Project{LastUpdatedDate: p.LastUpdatedDate, Name: p.Name, GitURL: p.GitURL})
	ok, p, duration = s.(*scheduler).nextProjectToUpdate()
	if !ok {
		t.Fatal("Scheduler with project must found a next project")
	}
	if p.Name != "test2" {
		t.Fatal("Scheduler must select valid project test2 instead of", p.Name)
	}
	if int(duration.Seconds()) > 94 && int(duration.Seconds()) < 96 {
		t.Fatal("Scheduler must sleep during 90sc before update project instead of", duration.Seconds())
	}
}

type fakeSender struct {
	call func(project.Project)
}

func (s fakeSender) Send(p project.Project) {
	s.call(p)
}

func (s fakeSender) Close() {}

type fakeDownloaderManager struct {
}

func (f fakeDownloaderManager) BuildProject(url string) (project.Project, error) {
	return project.Project{GitURL: url, Name: "test"}, nil
}

func (f fakeDownloaderManager) Download(project project.Project) (project.Project, error) {
	return project, nil
}

func TestStart(t *testing.T) {
	chanOk := make(chan project.Project, 1)
	sender := fakeSender{call: func(p project.Project) {
		chanOk <- p
	}}
	config := Config{Enabled: true, PathDb: DefaultConfig.PathDb, WaitDurationProcess: "1s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid")
	}
	s.(*scheduler).projects = []minimalProject{{LastUpdatedDate: time.Now().Add(-1 * time.Second), Name: "test", GitURL: "test"}}
	s.Start(sender, fakeDownloaderManager{})
	select {
	case p := <-chanOk:
		if p.Name != "test" {
			t.Fatal("Scheduler must select valid project test instead of", p.Name)
		}
	case <-time.After(5 * time.Second):
		t.Fatal("Scheduler must start new project chain before 5 seconds")
	}
}

func TestStop(t *testing.T) {
	chanOk := make(chan project.Project, 1)
	sender := fakeSender{call: func(p project.Project) {
		chanOk <- p
	}}
	config := Config{Enabled: true, PathDb: DefaultConfig.PathDb, WaitDurationProcess: "100s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid")
	}
	s.(*scheduler).projects = []minimalProject{{LastUpdatedDate: time.Now().Add(-1 * time.Second), Name: "test", GitURL: "test"}}
	s.Start(sender, fakeDownloaderManager{})
	go func() {
		time.Sleep(10 * time.Millisecond)
		s.Close()
	}()
	select {
	case <-chanOk:
		t.Fatal("Scheduler should not launch chain if we call Close()")
	case <-time.After(100 * time.Millisecond):
	}
}

func TestStore(t *testing.T) {
	chanOk := make(chan project.Project, 1)
	sender := fakeSender{call: func(p project.Project) {
		chanOk <- p
	}}
	json, err := json.Marshal([]project.Project{{LastUpdatedDate: time.Now().Add(-1 * time.Second), Name: "test", GitURL: "test"}})
	if err != nil {
		t.Fatal(err)
	}
	err = ioutil.WriteFile("/tmp/continuous-evolution-scheduler-test.json", json, 0644)
	if err != nil {
		t.Fatal(err)
	}
	s, err := NewScheduler(Config{Enabled: true, PathDb: "/tmp/continuous-evolution-scheduler-test.json", WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid")
	}
	s.Start(sender, fakeDownloaderManager{})
	select {
	case p := <-chanOk:
		if p.Name != "test" {
			t.Fatal("Scheduler must select valid project test instead of", p.Name)
		}
	case <-time.After(5 * time.Second):
		t.Fatal("Scheduler must start new project chain before 5 seconds")
	}
	s.Save(project.Project{LastUpdatedDate: time.Now().Add(-10 * time.Second), Name: "test2", GitURL: "test2"})
	s.Close()
	s, err = NewScheduler(Config{Enabled: true, PathDb: "/tmp/continuous-evolution-scheduler-test.json", WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid")
	}
	s.Start(sender, fakeDownloaderManager{})
	if len(s.(*scheduler).projects) != 2 {
		t.Fatal("test2 should be saved in file")
	}
	if s.(*scheduler).projects[0].GitURL != "test" {
		t.Fatal("test should be saved")
	}
	if s.(*scheduler).projects[1].GitURL != "test2" {
		t.Fatal("test2 should be saved")
	}
	s.Delete(project.Project{Name: "test2"})
	if len(s.(*scheduler).projects) != 1 {
		t.Fatal("test2 should be removed in file")
	}
	s.Close()
	s, err = NewScheduler(Config{Enabled: true, PathDb: "/tmp/continuous-evolution-scheduler-test.json", WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid")
	}
	s.Start(sender, fakeDownloaderManager{})
	s.Close()
	if len(s.(*scheduler).projects) != 1 {
		t.Fatal("test2 should be removed in file")
	}
	if s.(*scheduler).projects[0].GitURL != "test" {
		t.Fatal("test should be saved")
	}
}
