package deleter

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"fmt"
	"os/exec"

	"github.com/sirupsen/logrus"
)

var logger = logrus.WithField("logger", "deleter/deleter")

//Delete the directory of a project. Use docker to avoid right conflict (because some file are created/modified by docker).
func Delete(project project.Project) (project.Project, error) {
	if project.PathToWrite == "" || project.Name == "" {
		return project, fmt.Errorf("Can't delete path %s/%s", project.PathToWrite, project.Name)
	}
	volume := fmt.Sprintf("%s/%s", project.PathToWrite, project.Name)
	tools.NewDocker().StartDocker("alpine", []string{"rm", "-rf", "/data"}, []string{volume + ":/data"}, "/data", []string{}, []string{})
	cmd := exec.Command("rm", "-rf", project.Name)
	cmd.Dir = project.PathToWrite
	if err := cmd.Run(); err != nil {
		logger.WithField("path", volume).WithError(err).Error("Error making rm -rf")
		return project, err
	}
	return project, nil
}
