package orchestrator

import (
	"context"
	"continuous-evolution/src/project"
	"errors"
)

//Callback is the function called on each new project, also called worker
type Callback = func(project.Project) (project.Project, error)

//CallbackFinally is the callback called at the end of the chain
type CallbackFinally = func(project.Project, error)

//Sender represent a chain of callback and permit to send project.Project in it
type Sender interface {
	Send(project.Project)
	Close()
}

//WorkerConf represent a worker in the chain
type WorkerConf struct {
	Active   bool
	Callback Callback
	PoolSize int
}

type workerChain struct {
	project project.Project
	err     error
}

type worker struct {
	isListen bool
	callback Callback
	ctx      context.Context
	cancel   context.CancelFunc
	inChan   chan project.Project
	outChan  chan workerChain
}

//NewWorkers build a chain with each worker in order and add the finally callback at end
func NewWorkers(conf []WorkerConf, finally CallbackFinally) (Sender, error) {
	var first *worker
	var last *worker
	for _, c := range conf {
		if c.Active {
			if last == nil {
				last = newWorker(c.PoolSize, c.Callback)
				first = last
			} else {
				w, err := last.then(newWorker(c.PoolSize, c.Callback))
				if err != nil {
					return nil, err
				}
				last = w
			}
		}
	}
	last.finally(finally)
	return first, nil
}

func newWorker(nb int, callback Callback) *worker {
	ctx, cancel := context.WithCancel(context.Background())
	inChan := make(chan project.Project, 100) //TODO put in conf nb buffer
	outChan := make(chan workerChain, 100)    //TODO put in conf nb buffer
	w := &worker{callback: callback, ctx: ctx, cancel: cancel, inChan: inChan, outChan: outChan}
	for i := 0; i < nb; i++ {
		go func() {
			for {
				select {
				case p, ok := <-w.inChan:
					if ok {
						p, err := w.callback(p)
						w.outChan <- workerChain{project: p, err: err}
					}
				case <-w.ctx.Done():
					return
				}

			}
		}()
	}
	return w
}

func (w *worker) Send(project project.Project) {
	w.inChan <- project
}

func (w *worker) then(o *worker) (*worker, error) {
	if w.isListen {
		return w, errors.New("you have already use then or finally on this instance")
	}
	w.isListen = true
	go func() {
		for {
			select {
			case wc, ok := <-w.outChan:
				if ok && wc.err == nil {
					o.inChan <- wc.project
				} else if ok {
					o.outChan <- wc
				}
			case <-w.ctx.Done():
				o.Close()
				return
			}
		}
	}()
	return o, nil
}

func (w *worker) finally(callback CallbackFinally) (*worker, error) {
	if w.isListen {
		return w, errors.New("you have already use then or finally on this instance")
	}
	w.isListen = true
	go func() {
		for {
			select {
			case wc, ok := <-w.outChan:
				if ok {
					callback(wc.project, wc.err)
				}
			case <-w.ctx.Done():
				return
			}
		}
	}()
	return w, nil
}

func (w *worker) Close() {
	w.cancel()
}
