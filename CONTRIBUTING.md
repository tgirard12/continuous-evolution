# Contributing

First of all, thanks for taking the time to contribute!

# How to contribute

## Reporting Bugs

Bugs are tracked as [Gitlab issues](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues). Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

Explain the problem and include additional details to help maintainers reproduce the problem:

* Look at existing issues, if you find an already existing issue for your probem, don't hesitate to :+1: and add new details if possible.
* Use a clear and descriptive title for the issue to identify the problem.
* Detail your environment (windows, linux, mac...)
* Add the command line and/or the git repository to reproduce the problem.
* Explain the behavior you expect to see instead and why.

## Code contribution

Merge-request (or pull-request) are made at [Gitlab merge-request](https://gitlab.com/ContinuousEvolution/continuous-evolution/merge_requests).

Before implementing a new feature, please open an issue to discuss the impacts and the design.

The continuous-integration will check :

* That the code is well formatted : `make lint` to check
* That all tests are green, please add unit tests with your code : `make test` to run them

Obviously, a merge-request will be accepted only if pipeline is green.

## Other

If you have any question, please create an issue, we will answer as soon as possible.
