---
title: "Advanced usage"
menu: 
    main:
        weight: 20
        parent: "Develop"
subnav: "true"
description: "How to run easily ?"
---
# Advanced usage

Variable **$GIT_URL** in this page must be conform to :

* local : **/absoluate/local/path/projet/** please note slash at start AND end
* gitlab : **https://$GITLAB_USERNAME:$GITLAB_TOKEN@gitlab.com/$ORGANISATION/$PROJECT.git**
* github : **https://$GITHUB_USERNAME:$GITHUB_TOKEN@github.com/$ORGANISATION/$PROJECT.git**

## Docker launch

```bash
docker run -it --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /tmp:/tmp \
    registry.gitlab.com/continuousevolution/continuous-evolution -url $GIT_URL
```

## Web

```bash
docker run -it --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /tmp:/tmp \
    -v /absolute/path/to/localConf.toml:/config/localConf.toml \
    -p 1234:1234 \
    registry.gitlab.com/continuousevolution/continuous-evolution --config /config/localConf.toml
open `http://127.0.0.1:1234/addProject?project=$GIT_URL`
```

> Full example config can be found on [master/configuration.toml](https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/configuration.toml)
