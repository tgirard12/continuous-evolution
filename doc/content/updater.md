---
title: "Updater"
menu: 
    main:
        weight: 4
        parent: "Develop"
subnav: "true"
description: "Updater is the heart of continuous-evolution"
---
# Updater

## What is an updater ?

An updater permit to update dependencies of a dependencies manager. It must accept one type of file (package.json, pom.xml, Dockerfile...) and update it.

Ideally, it's a docker image when continuous-evolution call the entrypoint it update the file which is volumed and put in the current workdir. This entrypoint can take an argument to have excluded dependencies. And this entrypoint return a json containing all dependencies updated with name, old version and new version.

Of course, this kind of updater don't exist. So we need to add little code around existing updaters. As examples you can browse the [directory](https://gitlab.com/ContinuousEvolution/continuous-evolution/tree/master/src/updaters) contianing all updaters of continuous-evolution. 

## How to build my updater ?

### Link to an issue

First be sure no one do it by searching/opening an [issue](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues).

### Make a docker

The better way to do an updater, is to do an application in the language of your choice. And creating an image docker. After that you will be able to easily integrate your updater in continuous-evolution. In a futur, we want all updaters, will run by this ways (we will make a repo/docker for each of internal updaters).

Indeed, we think, if you do an npm updater, node is the better plateforme. If you do a cargo updater, rust is the better language. Anyway, what we want is a binary to update dependencies, not a war between languages ;)

So, the cli to build must respond to this rules :

* It work on the working directory of docker (current path)
* It accept --excludes to not update some dependencies, the list is comma separated (e.g --excludes=lib1,lib2)
* It log only the result, a json containing all dependencies updated with old and new version (of course you can add a --verbose to have more logs). Example :

```json
[{"name":"lib1", "newVersion":"0.1.0", "originalVersion":"0.0.1", "canBeExcludes":true}]
```

### Add it to Continuous-Evolution

> If you have a docker but don't know golang just open an issue and we will do it for you.

When your updater is ok, make a merge-request to Continuous-Evolution with :

* A new file in **src/updaters/YOUR_UPDATER.go**, this updater must implement [internalUpdater interface](https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/src/updaters/manager.go#L39) :
    * it has **func(pathString string, f os.FileInfo) bool**
    * and **Update(projectPath string, pkg model.Package, excludes []string) ([]model.Dependency, error)**, in this case the updater will launch you docker
* The [configuration.toml](https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/configuration.toml#L22) is updated with updater's configuration
* Add it in [landing.html](https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/doc/content/landing.html#L146) and in [Readme.md](https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/README.md#update)

